#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
    int i;
    int NUM=atoi(argv[1]);
    long long fib[NUM];
    fib[1]=1;
    for (i=2; i<=NUM; i++) {
        fib[i]=2*fib[i-1];
    }
    printf("%lli\n", fib[NUM]);
}
