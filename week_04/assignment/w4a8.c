#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
    int i;
    int NUM=atoi(argv[1]);
    long long fib[NUM];
    fib[1]=1;
    fib[2]=2;
    for (i=3; i<=NUM; i++) {
        fib[i] = fib[i-1] + fib[i-2];
    }
    printf("%lli\n", fib[NUM]);
}
