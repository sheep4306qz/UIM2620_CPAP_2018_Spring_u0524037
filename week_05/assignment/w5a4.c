#include <stdio.h>
#include <stdlib.h>
long double factorial( register unsigned int m); //function prototype

int main(int argc, char *argv[]){
    unsigned int n;
    if(argc<=1){
        n=5;
    }else{
        n=atoi(argv[1]);
    }
    printf("%.0Lf\n",factorial(n));


}

long double factorial( register unsigned int n )
{
    long double f = 1;
    while ( n > 1 )
    f *= n--;
    return f;
}
