#include<stdio.h>
#include<stdlib.h>
long long F(long long n){
    if(n==0){
        return 0;
    }else if(n==1){
        return 1;
    }else if(n==2){
        return 2;
    }
    else{
        return F(n-1)+F(n-2);
    }
}
int main(int argc ,char *argv[]){
    long long n;
    n=atoll(argv[1]);
    if(n<0){
        printf("E\n");
    }else{
        printf("%lld\n",F(n));
    }
}
