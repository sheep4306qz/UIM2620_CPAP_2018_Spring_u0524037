#include<stdio.h>
#include<stdlib.h>
int NumOfOne(int n){
    int number=0;
    while(n>0){
        if((n%2)!=0){
            number++;
            n/=2;
        }else{
            n/=2;
        }
    }
    return number;
}
int main(int argc,char *argv[]){
    int n=atoi(argv[1]);
    int number=NumOfOne(n);
    printf("%d\n",number);
}
