#include<stdio.h>
int main(void){
    int i,j;//i=行數 j=控制空白與星星數量
    for(i=1;i<=3;i++){ //上半部的菱形（包含中間對稱線）
        int space=(3*2-(i*2-1))/2;  //菱形一半的空白數量
        for(j=0;j<space;j++) printf(" ");//打印左半部空白
        for(j=0;j<(i*2-1);j++) printf("*");//依照行數不同打印不同數量的星星
        for(j=0;j<space;j++) printf(" ");//打印右半部空白
        printf("\n");//打完一行星星換行
    }
    for(i=2;i>=1;i--){ //下半部的菱形（不包含中間對稱線）
        int space=(3*2-(i*2-1))/2;  //菱形一半的空白數量
        for(j=0;j<space;j++) printf(" ");//打印左半部空白
        for(j=0;j<(i*2-1);j++) printf("*");//依照行數不同打印不同數量的星星
        for(j=0;j<space;j++) printf(" ");//打印右半部空白
        printf("\n");//打完一行星星換行
    }
}
