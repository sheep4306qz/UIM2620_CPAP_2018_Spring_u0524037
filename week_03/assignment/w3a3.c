#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main(int argc, char *argv[])
{
    int i;
    long int NUM;
    NUM=atoi(argv[--argc]);
    long fib[NUM];
    /*fib[0]=1;
    fib[1]=1;*/
    for(i=0;i<NUM;i++){
    	fib[i]=1;
	}
    for (i=2; i<NUM; i++) {
        fib[i] = fib[i-1] + fib[i-2];
    }
    for (i=0; i<NUM; i++) {
        printf("%d:%ld\n", i, fib[i]);
    }

    return 0;
}

